#!/usr/bin/env python3
import sys
import gi
gi.require_version('Gst', '1.0')
gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
del gi
from gi.repository import GObject, GLib, Gst

def bus_call(bus, message, loop):
	print(message)
	t = message.type
	if t == Gst.MessageType.EOS:
		sys.stdout.write("End-of-stream\n")
		playbin.set_state(Gst.State.NULL)
		loop.quit()
	elif t == Gst.MessageType.ERROR:
		err, debug = message.parse_error()
		print("Error: {}: {}".format(err, debug), file=sys.stderr)
		loop.quit()
	return True

Gst.init(sys.argv)
loop = GLib.MainLoop()
if 1:
	monitor = Gst.DeviceMonitor.new()
	monitor.add_filter("Audio/Sink", None)
	monitor.start()
	devices = monitor.get_devices()
	for device in devices:
		print('---')
		print('device.description', device.get_properties().get_value('device.description'))
		print('device.form_factor', device.get_properties().get_value('device.form_factor'))
		print('device.icon_name', device.get_properties().get_value('device.icon_name'))
		print('is-default', device.get_properties().get_value('is-default'))
		print('device.vendor.id', device.get_properties().get_value('device.vendor.id'))
		print('device.class', device.get_properties().get_value('device.class'))
		print('device.string', device.get_properties().get_value('device.string'))
	exit(0)
pb1 = Gst.ElementFactory.make("playbin", None)
pb1.set_property('uri', Gst.filename_to_uri('404910__brainclaim__dark-shepard-tone-3.wav'))
bus = pb1.get_bus()
bus.add_signal_watch()
bus.connect ("message", bus_call, loop)
pb1.set_state(Gst.State.PLAYING)
#
#pb2 = Gst.ElementFactory.make("playbin", None)
#pb2.set_property('uri', Gst.filename_to_uri('Industrial_Factory Fans-269594.mp3'))
#pb2.set_state(Gst.State.PLAYING)
loop.run()
