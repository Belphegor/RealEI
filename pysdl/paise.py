#!/usr/bin/env python3
# consider https://zach.se/generate-audio-with-python/
from itertools import *
from math import sin, pi
from pyaudio import PyAudio
from numpy import array

def sine_wave(frequency=440.0, framerate=44100):
	return (128 * sin(2.0*pi*frequency*(i/framerate)) + 128 for i in count(0))

p = PyAudio()
stream = p.open(format=p.get_format_from_width(1), channels=1, rate=44100, output=True)
stream.write(bytes(int(n) for n in islice(sine_wave(), 44100 * 5)))
stream.stop_stream()
stream.close()
p.terminate()
