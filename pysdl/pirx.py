#!/usr/bin/env python3
# see http://www.pygame.org/docs/
# see file:///usr/share/doc/python-pygame-doc/html/index.html
#
# Nacherzählung der Kurzgeschichte "Test" über den Piloten Pirx von Stanislav
# Lem.
#
from math import pi
from random import randint, choice
#from pygame_sdl2 import import_as_pygame; import_as_pygame() # doesnt work for now
from pygame import (
	FULLSCREEN,
	JOYBUTTONDOWN,
	JOYBUTTONUP,
	KEYUP,
	K_q,
	K_z,
	K_SPACE,
	NOFRAME,
	#NUMEVENTS,
	QUIT,
	USEREVENT,
	#camera,
	display,
	draw,
	event,
	font,
	image,
	init,
	joystick,
	mixer,
	mouse,
	quit,
	time,
	transform,
	)

TITLE = 'RV2000'
UE_PLAY_FOG = USEREVENT
UE_END_BG = USEREVENT + 1
COLOR_BLACK = (0, 0, 0, )
COLOR_STATUS = (10, 220, 10, )
COLOR_ALERT = (255, 0, 0, )
COLOR_ONE = (60, 60, 170, )
COLOR_TWO = (60, 170, 60, )
SIZE = 1280, 720, # 16:9
fullscreen = True

def draw_l(screen, rectangle, color, deco, width=50):
	screen = screen.subsurface(rectangle)
	_, _, dx, dy = rectangle
	draw.rect(screen, color, (0, 0, width, dy-width*2, ))
	draw.arc(screen, color, (0, dy-4*width, width*4, width*4, ), pi, pi/2*3+.1, width)
	draw.rect(screen, color, (0+width*2, dy-width, dx-width*2, width, ))
	sf = deco.render(TITLE, True, COLOR_BLACK)
	screen.blit(sf, (dx-sf.get_width()-25, dy-width+5), )

def calc_scale(moon_original, moon_scaled, size, altitude, **args):
	# w : d = moon_scale : altitude
	if altitude < 30:
		return None, None
	moon_scale = int(120000 / altitude)
	if not moon_scaled or moon_scaled.get_width() != moon_scale:
		moon_scaled = transform.smoothscale(moon_original, (moon_scale, moon_scale, ))
	return moon_scaled, (200-moon_scale//2+randint(0, 2), 200-moon_scale//2+randint(0, 2), ),

def descent(elapsed_time, speed, propellant, altitude, velocity, gravity, **args):
	delta_time = elapsed_time
	elapsed_time = time.get_ticks() / 1000.
	delta_time = elapsed_time - delta_time
	if altitude > 0:
		speed = speed + gravity * delta_time
		distance = delta_time * speed
	else:
		altitude = 0
		speed = 0
		distance = 0
	return dict(args,
		altitude = altitude - distance,
		elapsed_time = elapsed_time,
		gravity = gravity,
		propellant = propellant,
		speed = speed - velocity * delta_time,
		velocity = velocity,
		)

def paint_measures(subsurface, mono, measures):
	subsurface.fill((0, 0, 0, ))
	py = 0
	for text, value in measures.items():
		subsurface.blit(mono.render("{} {:.2f}".format(text, value, ), True, COLOR_STATUS), (0, py, ), )
		py += 40

def story(screen):
	mono = font.SysFont('Courier', 42, bold=True, italic=False)
	#mono = font.SysFont('iosevka', 30, bold=True, italic=False)
	#mono = font.SysFont('DejaVu Sans Mono', 30, bold=True, italic=False)
	deco = font.SysFont('helvetica', 30, bold=True, italic=False)
	measures = dict(
		altitude = 1200.0,
		elapsed_time = 0.0,
		gravity = 1.625, # see https://en.wikipedia.org/wiki/Gravitation_of_the_Moon
		propellant = 999.0,
		speed = 0.0,
		velocity = 0.0,
		)
	if True: # Intro ###################################################################################
		subsurface = screen.subsurface((20, 20, screen.get_width()-40, screen.get_height()-40, ))
		color = (10, 100, 10, )
		text = '''
Hallo Pilot Pirx!

Sie haben den theoretischen Teil ihrer
Ausbildung abgeschlossen. Heute beginnt
der praktische Teil mit einem Routine-
Flug zum Mond. Dieser Flug ist ein Test
auf ihre Belastbarkeit bei Flügen im
Weltall. Verhalten sie sich regelkonform.
Wir wünschen ihnen viel Erfolg.

Drücken sie die Leertaste, um den
Boardcomputer zu starten.
'''
		surface_copy = subsurface.copy()
		px = 0
		py = 0
		dy = 0
		for c in text:
			if c == '\n':
				px = 0
				py += dy
			elif ord(c) < 32:
				pass
			else:
				surface_text = mono.render(c, True, color)
				surface_copy.blit(surface_text, (px, py, ))
				dy = max(dy, surface_text.get_height())
				px += surface_text.get_width()
			subsurface.blit(surface_copy, (0, 0, ))
			e = yield False
		l = True
		while l:
			subsurface.blit(surface_copy, (0, 0, ))
			e = yield True
			if e and e.type == KEYUP and e.key == K_SPACE:
				l = False
		del surface_copy
		del subsurface
	surface_copy = screen.copy()
	draw_l(surface_copy, (300, 0, screen.get_width()-300, screen.get_height()-200, ), COLOR_ONE, deco, 50)
	leftcam = surface_copy.subsurface((400, 20, 400, 400, ))
	moon_original = image.load('images/apollo-17-full-moon-9.jpg')
	maschinenraum = image.load('images/maschinenraum-5401.jpg')
	maschinenraum = transform.smoothscale(maschinenraum, (700, 400, ))
	rightcam = surface_copy.subsurface((850, 20, 400, 400, ))
	moon_scaled = None
	if True: # Boot ####################################################################################
		l = True
		cam_scale = 400
		noise_b = image.load('images/b.png')
		noise_b = transform.smoothscale(noise_b, (cam_scale, cam_scale, ))
		noise_w = image.load('images/w.png')
		noise_w = transform.smoothscale(noise_w, (cam_scale, cam_scale, ))
		progress = 0
		while l and progress < screen.get_width():
			progress += 5
			if progress >= 200:
				paint_measures(surface_copy.subsurface((20, 20, 260, 510, )), mono, measures)
			if progress >= 400:
				if progress >= 600:
					leftcam.fill((0, 0, 0, ))
					moon_scaled, pos = calc_scale(moon_original, moon_scaled, 200, **measures)
					if moon_scaled:
						leftcam.blit(moon_scaled, pos)
				else:
					leftcam.blit(choice((noise_w, noise_b, )), (0, 0, ))
			if progress >= 500:
				if progress >= 700:
					rightcam.blit(maschinenraum, (0, 0, ))
				else:
					rightcam.blit(choice((noise_w, noise_b, )), (0, 0, ))
			draw.rect(surface_copy, COLOR_TWO, (0, 550, progress, 20, ))
			e = yield True
			screen.blit(surface_copy, (0, 0, ))
	l = True
	ssf = screen.subsurface((400, 600, screen.get_width()-400, screen.get_height()-600, ))
	rightcam.blit(maschinenraum, (0, 0, ))
	while l: # Descent #################################################################################
		e = yield True
		screen.blit(surface_copy, (0, 0, ))
		paint_measures(surface_copy.subsurface((20, 20, 260, 510, )), mono, measures)
		ssf.blit(mono.render('Press space to start descent.'.format(), True, COLOR_ALERT), (0+randint(0, 2), 0), )
		leftcam.fill((0, 0, 0, ))
		moon_scaled, pos = calc_scale(moon_original, moon_scaled, 200, **measures)
		if moon_scaled:
			leftcam.blit(moon_scaled, pos)
		if e and e.type == KEYUP and e.key == K_SPACE:
			l = False
	l = True
	measures['elapsed_time'] = time.get_ticks() / 1000.
	while l: # Intro ###################################################################################
		leftcam.fill((0, 0, 0, ))
		measures = descent(**measures)
		moon_scaled, pos = calc_scale(moon_original, moon_scaled, 200, **measures)
		if moon_scaled:
			leftcam.blit(moon_scaled, pos)
		paint_measures(surface_copy.subsurface((20, 20, 260, 510, )), mono, measures)
		screen.blit(surface_copy, (0, 0, ))
		e = yield True

if __name__ == '__main__':
	init()
	font.init()
	assert font.get_init()
	joystick.init()
	assert joystick.get_init()
	mixer.init()
	assert mixer.get_init()
	#mouse.init() # doesnt exist
	#assert mouse.get_init()
	#camera.init() # fails
	#assert camera.get_init()
	display.init()
	assert display.get_init()
	#info = display.Info()
	#print(display.list_modes())
	#print(font.get_fonts()); print(font.get_default_font()); print(font.match_font('iosevka', bold=True));exit(0)
	#screen = display.set_mode(flags=FULLSCREEN)
	screen = display.set_mode((1280, 720, ))
	try:
		mouse.set_visible(False)
		#cam = camera.Camera("/dev/video0",(640,480))
		#cam.start()
		#cam.query_image():
		#snapshot = surface.Surface(size, 0, display)
		#snapshot = cam.get_image(snapshot)
		if joystick.get_count():
			js = joystick.Joystick(0)
			js.init()
			assert js.get_init()
			print('joystick found', 'id', js.get_id(), 'name', js.get_name(), 'buttons', js.get_numbuttons(), 'hats', js.get_numhats(), )
		clock = time.Clock()
		sf = story(screen)
		sf.send(None)
		looping = True
		while looping:
			screen.fill((20, 20, 20, ))
			for e in event.get():
				if e.type == QUIT or e.type == KEYUP and e.key == K_q:
					print('quit')
					looping = False
				else:
					try:
						sf.send(e)
					except StopIteration:
						looping = False
			try:
				sf.send(None)
			except StopIteration:
				looping = False
			display.flip()
			#time.wait(30)
			#clock.get_time(),
			clock.tick(25)
	finally:
		font.quit()
		display.quit()
		quit()
		print('done')

