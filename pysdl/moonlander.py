#!/usr/bin/env python3
from time import sleep, clock_gettime, CLOCK_REALTIME

# horizontal speed 6000 km/h
# landing speed 2.5 ft/s down, 4ft/h forward
# 1.7m drop
# zentrifugalkraft = masse * radius * winkelgeschwindigkeit
# winkelgeschwindigkeit = :

def main():
	# See https://en.wikipedia.org/wiki/Apollo_11
	altitude = 1800.0
	propellant = 999
	speed = 0.0
	gravity = 1.625 # see https://en.wikipedia.org/wiki/Gravitation_of_the_Moon
	elapsed_time = clock_gettime(CLOCK_REALTIME)
	delta_time = 0.0
	velocity = 0.0
	while altitude > 0.0:
		delta_time = elapsed_time
		elapsed_time = clock_gettime(CLOCK_REALTIME)
		delta_time = elapsed_time - delta_time
		print(speed, 'Höhe', altitude)
		speed += gravity * delta_time
		speed -= velocity * delta_time
		distance = delta_time * speed
		altitude = altitude - distance
		sleep(3)

main()

exit(0)

from tkinter import *

def configure(event):
	#print('--- configure type={}, x={}, y={}, width={} height={}'.format(event.type, event.x, event.y, event.width, event.height, ))
	global master, width, height, canvas
	if event.width != width or event.height != height:
		width, height = event.width, event.height
		canvas.config(width=width, height=height, )
		canvas.pack()
		canvas.delete(ALL)

def tchange():
	canvas.after(500, tchange)

show = True
width, height = 200, 200
master = Tk()
#master.attributes('-fullscreen', True)
master.bind('<Configure>', configure)
for n in ('<Control-q>', '<Control-w>', '<Control-x>', '<Alt-F4>', 'q', "<Button-1>", ):
	master.bind_all(n, lambda _:master.quit())
master.bind_all('<space>', tchange)
master.bind_all('t', tchange)
#master.bind_all("<Button-1>", mchange)
canvas = Canvas(master, width=width, height=height, cursor='none', borderwidth=0, background='black', highlightthickness=0, )
if show:
	canvas.after(500, tchange)
mainloop()
exit(0)
