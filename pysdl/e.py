#!/usr/bin/env python3
from datetime import datetime
from threading import Thread
from time import sleep

def countdown(context):
	from espeak import espeak
	# espeak.Punctuation.All, espeak.Punctuation.Any, espeak.Punctuation.Custom,
	# espeak.Parameter.Capitals, espeak.Parameter.Pitch, espeak.Parameter.Punctuation, espeak.Parameter.Range, espeak.Parameter.Rate, espeak.Parameter.Volume, espeak.Parameter.Wordgap,
	# espeak.Gender.Female, espeak.Gender.Male, espeak.Gender.Unknown,
	event_str = {
		espeak.event_END: 'END',
		espeak.event_MARK: 'MARK',
		espeak.event_MSG_TERMINATED: 'MSG_TERMINATED',
		espeak.event_PHONEME: 'PHONEME',
		espeak.event_PLAY: 'PLAY',
		espeak.event_SENTENCE: 'SENTENCE',
		espeak.event_WORD: 'WORD',
		}
	def cb(event, sequence, a):
		print(event_str[event], a)
	espeak.set_voice("de+14")
	#espeak.set_SynthCallback(cb)
	for n in [str(10-m) for m in range(10)] + ["Zündung", ]:
		s = datetime.now()
		espeak.synth(n + '!')
		while espeak.is_playing():
			pass
		d = 1.0 - (datetime.now() - s).microseconds / 1000000
		if d > 0:
			sleep(d)

t = Thread(target=countdown, args=(None, ))
t.setDaemon(True)
t.start()
sleep(12)
