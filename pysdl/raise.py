#!/usr/bin/env python3
import math
import pyaudio
import numpy as np

p = pyaudio.PyAudio()
fs = 44100
duration = 5
f = 440.0
base = math.pow(2, 1/duration)

def huellkurve_u(n):
	return 1.0
	n = abs(n - fs * duration / 2)
	n = (fs * duration / 2) - n
	return n / (fs * duration / 2)

def huellkurve_n(n):
	n = abs(n - fs * duration / 2)
	return n / (fs * duration / 2)

def shift(n):
	return (n + (fs * duration / 2)) % (fs * duration)

samples = np.array([np.float32(
	math.sin(n * math.pow(base, n/fs) / fs * f) * huellkurve_n(n)
	#math.sin(shift(n) * math.pow(1.2)/fs * f) * huellkurve_u(n)
	) for n in range(fs * duration)])
#samples = np.array([np.float32( shift(n)) for n in range(fs * duration)])
#print(min(samples), max(samples), samples[0], samples[fs * duration // 2 -1], samples[fs * duration - 1])
#exit(0)
stream = p.open(format=pyaudio.paFloat32, channels=1, rate=fs, output=True)
stream.write(samples)
stream.stop_stream()
stream.close()
p.terminate()
