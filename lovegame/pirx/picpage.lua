-- ------------------------------ PicPage
local Base = require "base"
local PicPage = Base.extend(Base)

function PicPage.new(self)
	Base.new(self)
	Picture = require "picture"
	self.pictures = {}
	for i=1,5 do
		table.insert(self.pictures, Picture(
			"sheep.png",
			math.random(1, self.graphicswidth), math.random(1, self.graphicsheight)))
	end
	self.deltax = 0
	self.deltay = 0
end

function PicPage.update(self, dt)
	for i,picture in ipairs(self.pictures) do
		picture:update(dt, self.deltax, self.deltay)
	end
end

function PicPage.joystickaxis(self, joystick, axis, value)
	if joystick then
		if axis == 1 then
			self.deltax = value * 9
		elseif axis == 2 then
			self.deltay = value * 9
		end
	end
end

function PicPage.draw(self)
	love.graphics.setColor(1, 1, 1)
	love.graphics.clear(1, 1, 1)
	for i,picture in ipairs(self.pictures) do
		picture:draw()
	end
end

function PicPage.keypressed(self, key, scancode, isrepeat)
	if key == "space" then
		self.nextChapter(self)
	end
end

function PicPage.mousepressed(self, x, y, button, istouch, presses)
	if math.abs(x - self.graphicswidth / 2) + math.abs(y - self.graphicsheight / 2) < 50 then
		self.nextChapter(self)
	end
end

return PicPage
