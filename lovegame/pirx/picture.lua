local Picture = Object.extend(Object)

function Picture.new(self, name, px, py)
	self.image = love.graphics.newImage(name)
	self.px = px -- pos on screen
	self.py = py
	self.dx = math.random(60, 240) -- delta for movement (speed)
	self.dy = math.random(60, 240)
	self.ox = self.image:getWidth() / 2 -- origin for rotation
	self.oy = self.image:getHeight() / 2
end

function Picture.update(self, dt, gravityx, gravityy)
	if false then
		if self.px + self.ox >= love.graphics.getWidth() or self.px - self.ox <= 0 then
			self.dx = -self.dx
		end
		if self.py + self.oy >= love.graphics.getHeight() or self.py - self.oy <= 0 then
			self.dy = -self.dy
		end
		--
		self.px = self.px + self.dx * dt
		self.py = self.py + self.dy * dt
		self.degree = math.atan2(self.dy, self.dx) + math.pi / 2
	else
		if self.px < love.graphics.getWidth() and gravityx > 0 or self.px > 0 and gravityx < 0 then
			self.px = self.px + dt * gravityx * self.dx
		end
		if self.py < love.graphics.getHeight() and gravityy > 0 or self.py > 0 and gravityy < 0 then
			self.py = self.py + dt * gravityy * self.dy
		end
		if math.abs(gravityx) + math.abs(gravityy) > 1 then
			self.degree = math.atan2(gravityy, gravityx) + math.pi / 2
		end
	end
end

function Picture.draw(self)
	love.graphics.setColor(1, 0.78, 0.15, 0.8)
	-- love.graphics.rectangle("line", self.px-self.ox, self.py-self.oy, self.ox*2, self.oy*2)
	love.graphics.draw(self.image,
		self.px, self.py,
		self.degree,
		1, 1,
		self.ox, self.oy
		)
end

function Picture.checkCollision(self, other)
	local a_left = self.px - self.ox
	local a_right = self.px + self.ox
	local a_top = self.py - self.oy
	local a_bottom = self.py + self.oy

	local b_left = other.px - other.ox
	local b_right = other.px + other.ox
	local b_top = other.py - other.oy
	local b_bottom = other.py + other.oy

	return
		--If Red's right side is further to the right than Blue's left side.
		a_right > b_left and
		--and Red's left side is further to the left than Blue's right side.
		a_left < b_right and
		--and Red's bottom side is further to the bottom than Blue's top side.
		a_bottom > b_top and
		--and Red's top side is further to the top than Blue's bottom side then..
		a_top < b_bottom
end

return Picture
