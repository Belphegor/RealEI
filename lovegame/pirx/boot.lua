-- ------------------------------------------------- Boot
local Base = require "base"
local Boot = Base.extend(Base)

function Boot.new(self)
	Base.new(self)
	self.progress = 0
	self.fontsize = self.graphicswidth / 37
	success, self.font = pcall(love.graphics.newFont, 'media/ZurichBT-ExtraCondensed.ttf', self.fontsize)
	if not success then
		self.fontsize = self.graphicswidth / 45
		self.font = love.graphics.newFont('media/SairaExtraCondensed-Medium.ttf', self.fontsize)
	end
	self.starting = love.audio.newSource('media/392890__djlprojects__tape-start-139bpm-sync.wav', 'stream')
	self.starting:play()
	self.noise_1 = love.graphics.newImage("media/noise_bw.png")
	self.noise_2 = love.graphics.newImage("media/noise_c.png")
	self.moon = love.graphics.newImage("media/apollo-17-full-moon-9.jpg")
	self.engine = love.graphics.newImage("media/maschinenraum-5401.jpg")
	-- self.logo = love.graphics.newImage("RKA_Logo.svg")
	self.moon_width = self.moon:getWidth()
	self.altitude = 1200.0
	self.elapsed_time = 0.0
	self.gravity = 1.625 -- see https://en.wikipedia.org/wiki/Gravitation_of_the_Moon
	self.propellant = 999.0
	self.speed = 0.0
	self.thrust = 0.0
	self.velocity = 0.0
end

function Boot.update(self, dt)
	if self.progress < 100 then
		self.progress = self.progress + dt * math.random(3, 9)
	end
	if not self.onscreen or not self.starting:isPlaying() then
		self.starting:release()
		self.starting = love.audio.newSource('media/417518__martin-sadoux__starting-up-industrial-boiler.wav', 'stream')
		self.starting:play()
		self.onscreen = true
	end
end

function Boot.draw_I(self, x, y, wy, ...)
	local r = wy/2
	local re,g,b = love.graphics.getColor()
	x = x + r
	love.graphics.arc('fill', x, y+r, r, math.pi / 2, math.pi / 2 * 3)
	local i = 1
	while i < select('#', ...) do
		local dx, s = select(i, ...), select(i+1, ...)
		dx = dx - r/2
		love.graphics.rectangle('fill', x, y, dx, wy)
		if s then
			love.graphics.setColor(0, 0, 0)
			love.graphics.print(s, x+40, y)
			love.graphics.setColor(re,g,b)
		end
		i = i + 2
		x = x + dx + r/2
	end
end

function Boot.draw_X(self, x, y, dy, ...)
	local r = dy/2
	local re,g,b = love.graphics.getColor()
	x = x + r
	local r0 = r
	love.graphics.arc('fill', x, y+r, r, math.pi/2*3, math.pi/2)
	local i = 1
	while i < select('#', ...) do
		local dx, s = select(i, ...), select(i+1, ...)
		dx = dx - r - r0
		r0 = 0
		love.graphics.rectangle('fill', x, y, dx, dy)
		if s then
			love.graphics.setColor(0, 0, 0)
			love.graphics.print(s, x+40, y)
			love.graphics.setColor(re,g,b)
		end
		i = i + 2
		x = x + dx + r
	end
	x = x - r
	love.graphics.arc('fill', x, y+r, r, math.pi/2, -math.pi/2)
end

function Boot.draw_L(self, x, y, dx, dy, wx, wy)
	if wx < wy then r = wx else r = wy end
	if dx <= 0 then
		dx = self.graphicswidth - x + dx
	end
	if dy <= 0 then
		dy = self.graphicsheight - y + dy
	end
	love.graphics.rectangle('fill', x, y, wx, dy-wy)
	love.graphics.rectangle('fill', x+r, y+dy-wy, dx-r, wy)
	love.graphics.arc('fill', x+r, y+dy-r, r, math.pi / 2, math.pi)
	love.graphics.rectangle('fill', x+wx, y+dy-r-r, r, r)
	love.graphics.setColor(0, 0, 0)
	love.graphics.arc('fill', x+wx+r, y+dy-r-r, r, math.pi / 2, math.pi)
end

function Boot.cam(self, d, shake)
end

function Boot.draw(self)
	love.graphics.clear(0, 0, 0)
	if self.onscreen then
		love.graphics.setFont(self.font)
		love.graphics.setColor(.6, .6, 1)
		self:draw_L(self.graphicswidth/3, 0, -40, self.graphicsheight/2, 120, 20)
		love.graphics.setColor(1, .6, 0)
		local x = 30
		local y = 30
		self:draw_X(x, self.graphicsheight/2 + 40, 40, self.graphicswidth - 480, nil, 400, os.date())
		self:draw_I(x, y+0, 40, self.graphicswidth/3-50, "Velocity: "..tostring(self.velocity))
		love.graphics.setColor(.8, .4, 0)
		self:draw_I(x, y+50, 40, self.graphicswidth/3-140, "Speed: 0", 90, nil)
		love.graphics.setColor(1, .8, .6)
		local s, dx
		if self.progress < 100 then
			s = 'Booting · Emergencysystem · Coresystem · Sensorsystem · Engine ·'
			dx = self.progress * 8 + 80
		else
			s = 'System ready'
			dx = self.graphicswidth - 80
		end
		self:draw_X(x, self.graphicsheight/2 + 90, 40, dx, s)
		self:draw_I(x, y+100, 40, self.graphicswidth/3-50, "Altitude: "..tostring(self.altitude))
		self:draw_I(x, y+150, 40, self.graphicswidth/3-50, "Elapsed: "..tostring(self.elapsed_time))
		-- self:draw_I(x, y+200, 40, self.graphicswidth/3-50, "Gravity: "..tostring(self.gravity))
		self:draw_I(x, y+200, 40, self.graphicswidth/3-50, "Propellant: "..tostring(self.propellant))
		self:draw_I(x, y+250, 40, self.graphicswidth/3-50, "Thrust: "..tostring(self.thrust))
		-- start cams
		love.graphics.setColor(1, 1, 1)
		x = self.graphicswidth/3 + 120 + 80
		y = 30
		local d = self.graphicsheight / 2 - 90
		love.graphics.setScissor(x, y, d, d)
		love.graphics.translate(x, y)
		local image
		if self.progress > 40 then
			image = self.moon
			moon_scale = (120000 / self.altitude) / self.moon_width
			moon_origin = self.moon_width / 2
		else
			if math.random(0,1) == 1 then
				image = self.noise_1
			else
				image = self.noise_2
			end
			moon_scale = .7
			moon_origin = image:getWidth()/2
		end
		love.graphics.draw(image,
			d/2 + math.random(-1, 1), d/2 + math.random(-1, 1), -- position
			0, -- orientation
			moon_scale, moon_scale, -- scale
			moon_origin, moon_origin -- origin
			)
		love.graphics.setScissor()
		love.graphics.origin()
		x = x + d + 70
		love.graphics.setScissor(x, y, d, d)
		love.graphics.translate(x, y)
		if self.progress > 20 then
			image = self.engine
		else
			image = self.noise_2
		end
		love.graphics.draw(image,
			d/2, d/2,
			0,
			d/image:getWidth(), d/image:getHeight(),
			image:getWidth()/2, image:getHeight()/2
			)
		love.graphics.setScissor()
		love.graphics.origin()
	end
end

function Boot.cleanup(self)
	self.starting:stop()
	self.starting:release()
	self.starting = nil
end

function Boot.keypressed(self, key, scancode, isrepeat)
	if key == "space" then
		self.nextChapter(self)
	end
end

function Boot.touchpressed(self, id, x, y, dx, dy, pressure)
	self.nextChapter(self)
end

return Boot
