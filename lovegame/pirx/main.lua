-- --------------------------------------- main
local DEBUG
 -- .. _G._VERSION
function log(fmt, ...)
	if DEBUG then
		print(string.format(fmt, ...))
	end
end

local function log_js()
	local joysticks = love.joystick.getJoysticks()
	log("joystsicks:")
	if #joysticks > 0 then
		joystick = joysticks[1]
		if joystick:getAxisCount() < 2 then
			log("weniger als 2 achsen")
			joystick = nil
		else
			log("2 achsen gefunden")
		end
	else
		log("keine joystsicks")
	end
end

local story
local Base = require "base"
function Base.nextChapter(self)
	if chapter then
		love.audio.stop()
		love.graphics.reset()
		love.system.vibrate(.3)
		chapter:cleanup()
	end
	chapter = table.remove(story, 1)
	if chapter == nil then
		return chapter
	end
	chapter = chapter()
end

function love.load()
	DEBUG = false
	if DEBUG then io.stdout:setvbuf("no") end
	log_js()
	story = {}
	table.insert(story, require "textpirx")
	table.insert(story, require "boot")
	-- table.insert(story, require "physpage")
	-- table.insert(story, require "picpage")
	-- table.insert(story, require "sample")
	Base.nextChapter()
end

function love.update(dt)
	if chapter then
		chapter:update(dt)
	else
		love.event.quit(0)
	end
end

function love.draw()
	if chapter then
		chapter:draw()
	else
		love.graphics.clear(1, 0, 0)
	end
end

function love.keypressed(key, scancode, isrepeat)
	if key == "escape" then
		love.event.quit(0)
	elseif chapter then
		chapter:keypressed(key, scancode, isrepeat)
	end
end

function love.joystickaxis(joystick, axis, value)
	if chapter then
		chapter:joystickaxis(joystick, axis, value)
	end
end

function love.mousepressed(x, y, button, istouch, presses)
	if chapter then
		chapter:mousepressed(x, y, button, istouch, presses)
	end
end

function love.touchpressed(id, x, y, dx, dy, pressure)
	if chapter then
		chapter:touchpressed(id, x, y, dx, dy, pressure)
	end
end

function love.displayrotated(index, orientation)
	if chapter then
		chapter:displayrotated(index, orientation)
	end
end

