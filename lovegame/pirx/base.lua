-- -------------------------------------------- Base
Object = require "classic"
Base = Object.extend(Object)

function Base.new(self, nextchapter)
	self.graphicswidth = love.graphics.getWidth()
	self.graphicsheight = love.graphics.getHeight()
	self.nextchapter = nextchapter
end

function Base.cleanup(self)
	--
end

function Base.update(self, dt)
	--
end

function Base.draw(self)
	--
end

function Base.keypressed(self, key, scancode, isrepeat)
	log(string.format('key=%q', key))
end

function Base.joystickaxis(self, joystick, axis, value)
	log(string.format('joystick=%q, axis=%q, value=%q', joystick, axis, value))
end

function Base.mousepressed(self, x, y, button, istouch, presses)
	log(string.format('x=%q, y=%q, button=%q, istouch=%q, presses=%q', x, y, button, istouch, presses))
end

function Base.displayrotated(self, index, orientation)
	-- buggy...
	-- orientation = love.window.getDisplayOrientation(index)
	log(string.format('index=%q, orientation=%q', index, orientation))
end

function Base.touchpressed(id, x, y, dx, dy, pressure)
	log(string.format('id=%q, x=%q, y=%q, dx=%q, dy=%q, pressure=%q', id, x, y, dx, dy, pressure))
end

return Base
