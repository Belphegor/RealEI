-- ------------------------------ SamplePage
local Base = require "base"
local SamplePage = Base.extend(Base)

function SamplePage.new(self)
	Base.new(self)
end

function SamplePage.draw(self)
	love.graphics.clear(1, 0, 0)
end

function SamplePage.keypressed(self, key, scancode, isrepeat)
	if key == "space" then
		self.nextChapter(self)
	end
end

function SamplePage.mousepressed(self, x, y, button, istouch, presses)
	if math.abs(x - self.graphicswidth / 2) + math.abs(y - self.graphicsheight / 2) < 50 then
		self.nextChapter(self)
	end
end

return SamplePage

