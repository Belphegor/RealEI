-- ------------------------------------------------- TextPirx
local TextFlow = require "textflow"
local TextPirx = TextFlow.extend(TextFlow)

function TextPirx.new(self)
	TextFlow.new(self, [[
Hallo Pilot Pirx!

Sie haben den theoretischen Teil ihrer
Ausbildung abgeschlossen. Heute beginnt
der praktische Teil mit einem Routine-
Flug zum Mond. Dieser Flug ist ein Test
auf ihre Belastbarkeit bei Flügen im
Weltall.

Verhalten sie sich regelkonform.
  
Wir wünschen ihnen viel Erfolg.

Drücken sie die Leertaste, um den
Boardcomputer zu starten.
                       
                  
              
          
      
  
]])
end

return TextPirx
