-- ------------------------------------------------- TextFlow
local Base = require "base"
local TextFlow = Base.extend(Base)

local utf8 = require("utf8")
utf8.sub = function(s, i, j)
	if utf8.len(s) < 1 then return s end
	return utf8.char(utf8.codepoint(s, i, j))
end

local function gsplit(text, pattern, plain)
	local splitStart, length = 1, #text
	return function ()
		if splitStart then
			local sepStart, sepEnd = string.find(text, pattern, splitStart, plain)
			local ret
			if not sepStart then
				ret = string.sub(text, splitStart)
				splitStart = nil
			elseif sepEnd < sepStart then
				-- Empty separator!
				ret = string.sub(text, splitStart, sepStart)
				if sepStart < length then
					splitStart = sepStart + 1
				else
					splitStart = nil
				end
			else
				ret = sepStart > splitStart and string.sub(text, splitStart, sepStart - 1) or ''
				splitStart = sepEnd + 1
			end
			return ret
		end
	end
end

local function split(text, pattern, plain)
	local ret = {}
	for match in gsplit(text, pattern, plain) do
		table.insert(ret, match)
	end
	return ret
end

function TextFlow.new(self, message)
	Base.new(self)
	self.fontsize = self.graphicswidth / 25
	self.font = love.graphics.newFont('media/ShareTechMono-Regular.ttf', self.fontsize)
	self.tty = love.audio.newSource('media/9098__ddohler__typewriter_cut.wav', 'stream')
	self.cr = love.audio.newSource('media/318686__ramsamba__typewriter-carriage-return.wav', 'stream')
	self.lineno = 1
	self.charno = 1
	self.timer = 0
	self.delta = .07
	if message == nil then
		message = 'Hi wrld'
	end
	self.message = split(message, '\n', false)
end

function TextFlow.update(self, dt)
	self.timer = self.timer + dt
	if self.timer > self.delta then
		self.timer = 0
		self.delta = math.random(40, 140) / 1000
		self.tty:stop() -- stop wenn es bereits laeuft
		self.tty:play()
		self.charno = self.charno + 1
		if self.lineno > #self.message or self.charno > utf8.len(self.message[self.lineno]) then
			self.charno = 1
			self.lineno = self.lineno + 1
			if self.lineno > #self.message + 40 then
				self.nextChapter()
			end
		elseif self.lineno <= #self.message and self.charno >= utf8.len(self.message[self.lineno]) then
			self.cr:stop() -- stop wenn es bereits laeuft
			self.cr:play()
			self.delta = self.cr:getDuration() - .3
		end
	end
end

function TextFlow.draw(self)
	local d = .1
	love.graphics.clear(d, d, d)
	love.graphics.setFont(self.font)
	love.graphics.setColor(0, .9, 0)
	y = self.graphicsheight - 10 - (self.fontsize*1.1) * self.lineno
	for i,msg in ipairs(self.message) do
		if i < self.lineno then
			love.graphics.print(msg, 10, y)
			y = y + (self.fontsize*1.1)
		end
	end
	if self.lineno <= #self.message then
		local line = self.message[self.lineno]
		utf8.len(line)
		line = utf8.sub(line, 1, self.charno)
		love.graphics.print(line, 10, self.graphicsheight-10-self.fontsize)
	end
end

function TextFlow.cleanup(self)
	self.cr:stop()
	self.cr:release()
	self.cr = nil
	self.tty:stop()
	self.tty:release()
	self.tty = nil
end

function TextFlow.keypressed(self, key)
	if key == "space" then
		self.nextChapter(self)
	end
end

function TextFlow.mousepressed(self, x, y, button, istouch, presses)
	self.nextChapter(self)
end

return TextFlow
